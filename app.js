var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var restAPI = require('./REST_API/index');

var app = express();
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', restAPI);

// catch error on bad url request.
app.use(function(req, res, next) {
    return res.status(400).json({error: "path not found"});
});

module.exports = app;
