# API AVATARS

REST API, generates generates and return .svg images based on user gender and name.

## Documentation

* GET /v1/custom-avatar?gender={gender}&name={name}  
    --> returns .svg image.  
* GET /  
    --> returns api info.  
* GET /health  
  --> return health status  
