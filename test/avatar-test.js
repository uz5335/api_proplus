var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();

chai.use(chaiHttp);

describe('Avatars', function() {
  it('should generate new avatar for user /v1/custom-avatar POST', function(done) {
  chai.request(server)
    .get('/v1/custom-avatar?gender=F&name=kfgf')
    .end(function(err, res){
      res.should.have.status(200);
      res.should.be.object;
      res.should.be.svg;
      done();
    });
});

it('should generate throw exeption for not given enough params, gender not definet', function(done) {
chai.request(server)
  .get('/v1/custom-avatar?gender=&name=kfgf')
  .end(function(err, res){
    res.should.have.status(400);
    res.should.be.json;
    res.body.should.have.property('error');
    res.body.error.should.equal('no gender or name given');
    done();
  });
});

it('should generate throw exeption for not given enough params, name not definet', function(done) {
chai.request(server)
  .get('/v1/custom-avatar?gender="F"&name=')
  .end(function(err, res){
    res.should.have.status(400);
    res.should.be.json;
    res.body.should.have.property('error');
    res.body.error.should.equal('no gender or name given');
    done();
  });
});
it('wrong path', function(done) {
chai.request(server)
  .get('/v2/custom-avatar?gender="F"&name=')
  .end(function(err, res){
    res.should.have.status(400);
    res.should.be.json;
    res.body.should.have.property('error');
    res.body.error.should.equal('path not found');
    done();
  });
});

it('new avatar creation M', function(done) {
chai.request(server)
  .get('/v1/custom-avatar?gender=M&name=dsad')
  .end(function(err, res){
    res.should.have.status(200);
    res.should.be.object;
    res.should.be.svg;
    done();
  });
});
it('new avatar creation F', function(done) {
chai.request(server)
  .get('/v1/custom-avatar?gender=F&name=dsčcn')
  .end(function(err, res){
    res.should.have.status(200);
    res.should.be.object;
    res.should.be.svg;
    done();
  });
});

it('no gender', function(done) {
chai.request(server)
  .get('/v1/custom-avatar?name=dsčcn')
  .end(function(err, res){
    res.should.have.status(400);
        res.should.be.json;
    res.body.should.have.property('error');
    res.body.error.should.equal('no gender or name given');
    done();
  });
});
it('no name', function(done) {
chai.request(server)
  .get('/v1/custom-avatar?gender=F')
  .end(function(err, res){
    res.should.have.status(400);
    res.should.be.json;
    res.body.should.have.property('error');
    res.body.error.should.equal('no gender or name given');
    done();
  });
});

it('health test', function(done) {
chai.request(server)
  .get('/health')
  .end(function(err, res){
    res.should.have.status(200);
    res.body.should.be.String;
    //res.body,should.equal("healthy");
    done();
  });
});
it('API test info..', function(done) {
chai.request(server)
  .get('/')
  .end(function(err, res){
    res.should.have.status(200);
    res.should.be.json;

    res.body.should.be.a('object');
    res.body.should.have.property('error');
    res.body.error.should.equal(false);

    res.body.should.have.property('data');
    res.body.data.should.be.a('object');
    res.body.data.should.have.property('service');
    res.body.data.service.should.equal("avatar-generator");
    res.body.data.should.have.property('version');
    res.body.data.version.should.equal("1.0.0");
    res.body.data.should.have.property('platform');
    res.body.data.platform.should.equal("NodeJS 8.11.3");
    done();
  });
});


});
