/**
 *  === Application Bootstrapping ===
 */
var express = require("express");
var router = express.Router();

require('babel-register')({
    presets: [ 'env' ]
});
/**
 * Require controllers here
 */
let getAvatar = require('./controllers/avatarController');
let apiController = require('./controllers/apiController');
/**
 * Routes Registration
 */
router.get("/", apiController.getAPI);
router.get("/v1/custom-avatar", getAvatar.getAvatar);
router.get("/health", apiController.getHealth);
/**
 * Export router
 */
module.exports = router;
