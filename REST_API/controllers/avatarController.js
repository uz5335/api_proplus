import { avatar } from '../services/avatarService.js';

export function getAvatar(req, res) {
    const gender = req.query.gender;
    const name = req.query.name;
    if (!gender || !name) {
        return res.status(400).json({error: "no gender or name given"})
    }
    //console.log(avatar(gender, name));
    res.status(200).send(avatar(gender, name));
}
