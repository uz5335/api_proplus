/**
REST API info
**/
const apiInfo = function(){
  return {
      error: false,
      message: "",
      data:{
        service: "avatar-generator",
        version: "1.0.0",
        platform: "NodeJS 8.11.3"
      }
  }
}
module.exports.getAPI = function(req, res){
  res.status(200).json(apiInfo());
}
/*
health status
*/
module.exports.getHealth = function(req, res){
  res.status(200).send("healthy");
}
