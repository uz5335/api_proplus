import Avatars from '@dicebear/avatars';
import  SpriteCollectionMale  from '@dicebear/avatars-male-sprites';
import  SpriteCollectionFemale  from '@dicebear/avatars-female-sprites';
import  SpriteCollectionID  from '@dicebear/avatars-identicon-sprites';

export function avatar(gender, name) {
  switch (gender) {
    case "M":
          return (new Avatars(SpriteCollectionMale)).create(name);
    case "F":
        return (new Avatars(SpriteCollectionFemale)).create(name);
    default:
          return (new Avatars(SpriteCollectionID)).create(name);
  }
}
